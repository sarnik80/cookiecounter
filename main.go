package main

import (
	"io"
	"log"
	"net/http"
	"strconv"
)

func main() {

	http.HandleFunc("/", foo)
	http.ListenAndServe(":8080", nil)

}

func foo(w http.ResponseWriter, r *http.Request) {

	cookie, err := r.Cookie("my-value")

	if err == http.ErrNoCookie {

		cookie = &http.Cookie{

			Name:  "my-value",
			Value: "0",
			Path:  "/",
		}

		http.SetCookie(w, cookie)

	}

	count, err := strconv.Atoi(cookie.Value)

	if err != nil {
		log.Fatalln(err)
	}

	count++

	cookie.Value = strconv.Itoa(count)

	http.SetCookie(w, cookie)

	io.WriteString(w, cookie.Value)

}
